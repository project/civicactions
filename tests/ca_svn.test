<?php
class CASvnTest extends DrupalTestCase {
  function get_info() {
    return array(
      'name' => t('SVN Tests'),
      'desc' => t('makes sure that all files are checked out of SVN properly.'),
      'group' => 'CivicActions'
    );
  }
  
  function setUp() {
  }

  function testSVN() {
    // save the document root
    $this->root = $_SERVER['DOCUMENT_ROOT'];
    if (substr($this->root, -1) == '/') {
      $this->root = substr($this->root, 0, strlen($this->root) -1);
    }
    $this->root .= base_path();

    // save the civicrm root
    if (module_exists('civicrm')) {
      // the module is actually in a subdir, and we want the parent dir
      $parts = explode('/', drupal_get_path('module', 'civicrm'));
      array_pop($parts);
      $civicrm = implode('/', $parts);
    }

    // read svn files in the Drupal installation
    $this->file_scan_dirs('.', $drupal_svn, $drupal_nosvn);

    // check for Drupal installation errors
    $error_nosvn = array();
    if (count($drupal_svn)) {
      $this->check_SVN($drupal_svn);
    }
    elseif (count($drupal_nosvn)) {
      $error_nosvn[] = 'Drupal';
    }
    else {
      $error_nosvn += $this->unique_dirs($drupal_nosvn);
    }

    // read and check files in the sites directory
    $sites = conf_path();
    $this->file_scan_dirs($sites, $sites_svn, $sites_nosvn);
    $this->check_SVN($sites_svn, $civicrm);
    $error_nosvn += $this->unique_dirs($sites_nosvn);

    // read and check files in the civicrm directory
    $this->file_scan_dirs($civicrm, $civicrm_svn, $civicrm_nosvn);
    $this->check_SVN($civicrm_svn);
    $error_nosvn += $this->unique_dirs($civicrm_nosvn);

    // display one error about all files not checked out of SVN
    unset($error_nosvn['files']);
    unset($error_nosvn[$sites .'/files']);
    if (isset($civicrm)) {
      unset($error_nosvn[$civicrm]);
    }
    if (count($error_nosvn)) {
      $this->assertTrue(FALSE, substr(implode(', ', $error_nosvn), 0, 256) .' '. format_plural(count($error_nosvn), 'is', 'are') .' checked out of SVN');
    }
    else {
      $this->assertTrue(TRUE, 'All files are checked out of SVN');
    }
  }

  function file_scan_dirs($dir, &$svn, &$nosvn) {
    $full_dir = $this->root;
    if ($dir != '.') {
      $full_dir .= $dir;
    }

    $svn_entries = $full_dir .'/.svn/entries';
    if (file_exists($svn_entries)) {
      $entries = file($svn_entries);
      $svn[$dir] =  rtrim($entries[4]);
    }
    else {
      $nosvn[$dir] =  $dir;
    }

    if ($handle = opendir($full_dir)) {
      while ($file = readdir($handle)) {
        if ($file[0] != '.' && ($dir != '.' || $file != 'sites')) {
          $filename = $dir == '.' ? $file : $dir .'/'. $file;
          if (is_dir($this->root .'/'. $filename)) {
            $this->file_scan_dirs($filename, $svn, $nosvn);
          }
          else {
            // TODO: ???
          }
        }
      }
      closedir($handle);
    }
  }

  function check_SVN($svn, $ignore = '') {
    if (isset($svn) && count($svn)) {
      $error = array();
      foreach ($svn as $dir => $http) {
        if (!isset($svn_root)) {
          $svn_root = $http;
          $svn_root_len = strlen($svn_root);
          $svn_root_dir = $dir;
        }
        else {
          if (substr($http, 0, $svn_root_len) != $svn_root) {
            $error[$dir] = $dir;
          }
        }
      }

      $this->assertFALSE(preg_match(',/trunk$|/trunk/,', $svn_root), $svn_root_dir .' is a tagged SVN version, and not trunk');

      if (count($error)) {
        $error = $this->unique_dirs($error);
        if (!empty($ignore)) {
          unset($error[$ignore]);
        }
      }
      if (count($error)) {
        $this->assertTrue(FALSE, 'Some directories ('. substr(implode(', ', $error), 0, 256) .') have a different SVN root than '. $svn_root);
      }
      else {
        $this->assertTrue(TRUE, 'All directories under '. $svn_root_dir .' have the same SVN root');
      }
    }
  }

  function unique_dirs($dirs) {
    $unique_dirs = array();
    if (isset($dirs) && count($dirs)) {
      foreach ($dirs as $dir) {
        $parts = explode('/', $dir);
        $path = '';
        foreach ($parts as $part) {
          $path .= (empty($path) ? '' : '/') . $part;
          if (isset($unique_dirs[$path])) {
            continue 2;
          }
        }
        $unique_dirs[$dir] = $dir;
      }
    }
    return $unique_dirs;
  }
}

