
This is the basic module we use on all our client sites.  Feel free to use it on
your sites too, however, be warned that it's written specific for our client
setup.

It currently only hosts our simpletests, it's possible that the module will do
more in the future.  Most of the simpletests are generic enough to be used by
everyone.  But be warned that some are not.
